hdc shell mount -o remount,rw /vendor

hdc file send python3 /vendor/bin/
hdc shell chmod 777 /vendor/bin/python3

hdc file send pip3 /vendor/bin/
hdc shell chmod 777 /vendor/bin/pip3

hdc shell rm -rf /vendor/lib/python3.8
hdc shell mkdir -p /vendor/lib
hdc file send python3.8.tar /data
hdc shell tar -xvf /data/python3.8.tar -C /vendor/lib/
hdc shell rm -rf /data/python3.8.tar

REM 修改site.py相关配置
hdc shell rm -rf /data/python/lib
hdc shell mkdir -p /data/python/lib/python3.8
hdc file send site-packages.tar /data
hdc shell tar -xvf /data/site-packages.tar -C /data/python/lib/python3.8
hdc shell rm -rf /data/site-packages.tar

REM hdc shell "export PYTHONHOME=/vendor/bin"
REM hdc shell "export PYTHONPATH=/vendor"